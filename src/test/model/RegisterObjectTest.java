package test.model;

import com.rocket.app.model.Register;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

public class RegisterObjectTest {

    private Register register;
    private Map<Integer, Integer> testParameter;

    @Before
    public void setup() {
        register = new Register();
        testParameter = new TreeMap<>();
        register.setBillCount(testParameter);
    }

    @Test
    public void test() {
        assertEquals(testParameter, register.getBillCount());
    }
}
