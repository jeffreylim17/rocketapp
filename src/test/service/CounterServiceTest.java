package test.service;

import com.rocket.app.model.Register;
import com.rocket.app.service.CounterService;
import com.rocket.app.service.impl.CounterServiceImpl;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;
import java.util.Collections;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CounterServiceTest {

    private Register register;
    private CounterService counterService;
    private String showTestData;
    private String putTestData;
    private String takeTestData;
    private String changeTestData;

    @Before
    public void setup() {
        register = new Register();
        register.setBillCount(new TreeMap<>(Collections.reverseOrder()));
        register.getBillCount().put(1, 5);
        register.getBillCount().put(2, 4);
        register.getBillCount().put(5, 3);
        register.getBillCount().put(10, 2);
        register.getBillCount().put(20, 1);
        counterService = new CounterServiceImpl(register);

        showTestData = "$68 1 2 3 4 5 ";
        putTestData = "$128 2 4 6 4 10 ";
        takeTestData = "$43 1 0 3 4 0 ";

    }

    @Test
    public void testAShow() {
        /** initial data same as sample **/
        assertEquals(showTestData, counterService.show(register));
    }

    @Test
    public void testBPut() {
        assertEquals(putTestData, counterService.put(Arrays.asList(1, 2, 3, 0, 5)));
    }

    @Test
    public void testCTake() {
        counterService.put(Arrays.asList(1, 2, 3, 0, 5));
        assertEquals(takeTestData, counterService.take(Arrays.asList(1, 4, 3, 0, 10)));
    }

    @Test
    public void testDTake() {
        Register reg = counterService.change(67);
        assertEquals(1, reg.getBillCount().get(1).intValue());
        assertEquals(0, reg.getBillCount().get(2).intValue());
        assertEquals(0, reg.getBillCount().get(5).intValue());
        assertEquals(0, reg.getBillCount().get(10).intValue());
        assertEquals(0, reg.getBillCount().get(20).intValue());
    }

}
