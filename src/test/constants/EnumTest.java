package test.constants;

import com.rocket.app.constants.ActionEnum;
import org.junit.Test;

import static com.rocket.app.constants.ActionEnum.getEnum;
import static org.junit.Assert.assertEquals;

public class EnumTest {

    @Test
    public void test() {
        assertEquals("SHOW", ActionEnum.SHOW.getValue());
        assertEquals("PUT", ActionEnum.PUT.getValue());
        assertEquals("TAKE", ActionEnum.TAKE.getValue());
        assertEquals("CHANGE", ActionEnum.CHANGE.getValue());
        assertEquals("QUIT", ActionEnum.QUIT.getValue());
        assertEquals("DEFAULT", ActionEnum.DEFAULT.getValue());
    }

    @Test
    public void defaultTest() {
        assertEquals("DEFAULT", getEnum("1").getValue());
        assertEquals("DEFAULT", getEnum("A").getValue());
        assertEquals("DEFAULT", getEnum("#").getValue());
    }
}
