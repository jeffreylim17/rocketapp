package com.rocket.app.constants;

public enum ActionEnum {
    SHOW("SHOW"),
    PUT("PUT"),
    TAKE("TAKE"),
    CHANGE("CHANGE"),
    QUIT("QUIT"),
    DEFAULT("DEFAULT");

    private final String value;

    ActionEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ActionEnum getEnum(String value) {
        for (ActionEnum a : values()) {
            if (a.getValue().equalsIgnoreCase(value)) {
                return a;
            }
        }
        return DEFAULT;
    }
}
