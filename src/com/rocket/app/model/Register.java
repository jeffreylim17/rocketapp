package com.rocket.app.model;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Register implements Cloneable {

    private Map<Integer, Integer> billCount;

    public Register() {
    }

    public Register(Map<Integer, Integer> billCount) {
        Map<Integer, Integer> billC = new TreeMap<>(Collections.reverseOrder());
        billC.putAll(billCount);
        this.billCount = billC;
        return;
    }

    public Map<Integer, Integer> getBillCount() {
        return billCount;
    }

    public void setBillCount(Map<Integer, Integer> billCount) {
        this.billCount = billCount;
    }
}
