package com.rocket.app;

import com.rocket.app.service.OrchestratorService;
import com.rocket.app.service.impl.OrchestratorServiceImpl;

public class Application {

    private OrchestratorService orchestratorService;

    public static void main(String[] args) {
        System.out.println("ready");
        OrchestratorService orchestratorService = new OrchestratorServiceImpl();
        orchestratorService.start();
    }
}
