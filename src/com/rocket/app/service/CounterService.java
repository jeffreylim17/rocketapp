package com.rocket.app.service;

import com.rocket.app.model.Register;

import java.util.List;

public interface CounterService {

    String show(Register register);

    int total(Register register);

    String put(List<Integer> billCount);

    String take(List<Integer> billCount);

    Register change(int change);
}

