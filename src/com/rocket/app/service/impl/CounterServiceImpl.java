package com.rocket.app.service.impl;

import com.rocket.app.model.Register;
import com.rocket.app.service.CounterService;

import java.util.List;
import java.util.Map;

public class CounterServiceImpl implements CounterService {
    private static final String SPC = " ";
    private Register register;

    public CounterServiceImpl(Register register) {
        this.register = register;
    }

    @Override
    public String show(Register register) {
        this.register = register;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("$").append(total(register)).append(SPC);
        register.getBillCount().forEach((k, v) -> stringBuilder.append(v).append(SPC));
        return stringBuilder.toString();
    }

    @Override
    public int total(Register register) {
        int total = register.getBillCount().entrySet().stream().mapToInt(bill -> bill.getKey() * bill.getValue()).sum();
        return total;
    }

    @Override
    public String put(List<Integer> billCount) {
        int counter = 0;
        for (Map.Entry<Integer, Integer> bill : this.register.getBillCount().entrySet()) {
            bill.setValue(bill.getValue() + billCount.get(counter));
            counter++;
        }
        return show(this.register);
    }

    @Override
    public String take(List<Integer> billCount) {
        boolean isSuccess = true;
        Register stagingRegister = new Register(this.register.getBillCount());
        int counter = 0;
        for (Map.Entry<Integer, Integer> bill : stagingRegister.getBillCount().entrySet()) {
            if (!(bill.getValue() - billCount.get(counter) < 0)) {
                bill.setValue(bill.getValue() - billCount.get(counter));
            } else {
                isSuccess = false;
                continue;
            }
            counter++;
        }

        if (isSuccess)
            this.register.setBillCount(stagingRegister.getBillCount());
        else
            return "Sorry!";
        return show(this.register);
    }

    @Override
    public Register change(int change) {
        int stagingChange = change;
        StringBuilder sb = new StringBuilder();
        Register stagingRegister = new Register(this.register.getBillCount());
        for (Map.Entry<Integer, Integer> bill : stagingRegister.getBillCount().entrySet()) {
            int billCount = 0;
            while (stagingChange != 0) {
                if (stagingChange > total(stagingRegister)) {
                    break;
                } else if (checkValid(stagingChange, bill.getKey(), stagingRegister) && !bill.getValue().equals(0)) {
                    stagingChange = stagingChange - bill.getKey();
                    bill.setValue(bill.getValue() - 1);
                    billCount++;
                } else {
                    break;
                }
            }
            sb.append(billCount).append(SPC);
        }

        if (stagingChange != 0) {
            System.out.println("sorry");
        } else {
            System.out.println(sb.toString());
            this.register = stagingRegister;
        }
        return this.register;
    }

    private boolean checkValid(int change, int bill, Register stagingRegister) {
        if (bill <= change) {
            int remainder = change - bill;
            if (stagingRegister.getBillCount().get(bill) == 0)
                return false;
            if (remainder == 0)
                return true;
            else {
                return stagingRegister.getBillCount().entrySet().stream().filter(e -> e.getKey() <= remainder && e.getValue() != 0).findFirst().isPresent();
            }
        }
        return false;
    }
}
