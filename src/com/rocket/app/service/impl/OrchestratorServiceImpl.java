package com.rocket.app.service.impl;

import com.rocket.app.constants.ActionEnum;
import com.rocket.app.model.Register;
import com.rocket.app.service.CounterService;
import com.rocket.app.service.OrchestratorService;

import java.util.*;
import java.util.stream.Collectors;

import static com.rocket.app.constants.ActionEnum.QUIT;

public class OrchestratorServiceImpl implements OrchestratorService {

    private Register register;
    private CounterService counterService;

    public void start() {
        init();
        Scanner input = new Scanner(System.in);
        List<String> loaded = new ArrayList<>();
        loaded.add("show");

        while (!ActionEnum.getEnum(loaded.get(0)).equals(QUIT)) {
            List<Integer> payload;
            System.out.print(">");
            String result = "";
            loaded = Arrays.asList(input.nextLine().split(" "));
            switch (ActionEnum.getEnum(loaded.get(0))) {
                case SHOW:
                    result = counterService.show(register);
                    break;
                case PUT:
                    payload = extractPayload(loaded);
                    if (payload.size() == 5)
                        result = counterService.put(payload);
                    else
                        result = "Invalid input";
                    break;
                case TAKE:
                    payload = extractPayload(loaded);
                    if (payload.size() == 5)
                        result = counterService.take(payload);
                    else
                        result = "Invalid input";
                    break;
                case CHANGE:
                    register = counterService.change(Integer.valueOf(loaded.get(1)));
                    break;
                case QUIT:
                    result = "Bye";
                    break;
                default:
                    result = "Invalid input";
                    System.out.println(result);
                    continue;
            }
            /** display output **/
            System.out.println(result);
        }
    }

    private List<Integer> extractPayload(List<String> loaded) {
        return loaded.stream()
                .filter(v -> v.chars().allMatch(Character::isDigit))
                .map(Integer::parseInt).collect(Collectors.toList());
    }

    private void init() {
        /** initial value is set same as the example $20x1 $10x2 $5x3 $2x4 $1x5 **/
        register = new Register();
        register.setBillCount(new TreeMap<>(Collections.reverseOrder()));
        register.getBillCount().put(1, 0);
        register.getBillCount().put(2, 0);
        register.getBillCount().put(5, 0);
        register.getBillCount().put(10, 0);
        register.getBillCount().put(20, 0);
        counterService = new CounterServiceImpl(register);
    }
}
